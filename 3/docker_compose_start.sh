echo "---stopping the running docker---"
docker kill | docker ps -q
echo "---stopping the running docker completed---"
echo "---starting the docker---"
docker compose up -d
echo "---starting the docker completed---"
echo "---connecting to zerotier network---"
docker exec zerotier-one zerotier-cli join 8056c2e21c35d12b
echo "---connecting to zerotier network completed---"