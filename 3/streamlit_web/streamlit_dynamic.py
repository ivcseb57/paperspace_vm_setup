# python3 -m  streamlit run streamlit_dynamic.py 
import streamlit as st
import pandas as pd
import numpy as np
#top upper circuit
import pandas as pd
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
import pytz    
tz_NY = pytz.timezone('Asia/Kolkata')  
import redis
import time
import altair as alt

#pd.set_option('display.max_rows', None)
r = redis.Redis(host='redis_stack', port=6379, db=0)

st.set_page_config(
    page_icon='https://i.ibb.co/rcMt9H1/candlestick-chart.png',
    page_title='NSE_NFO : Raaghul Stock Market Real-Time Dashboard',
    layout='wide'
)
start_button = st.empty()
datetime_NY = datetime.now(tz_NY)  
indian_time =  datetime_NY.strftime("%d_%m_%Y_%H_%M_%S_%f")
df_indian_time =  datetime_NY.strftime("%d%m%Y %H:%M:%S")
#print("India time:",indian_time)
streamlit_refresh_time = st.empty()
redis_refresh_time = st.empty()
def write_update_text():
    streamlit_refresh_time.write("StreamLite Refreshed Time : " + indian_time)
    redis_refresh_time.write("Redis Last Insert Time : " + str(r.get("TICK_LAST_INSERT_TIME"),'utf-8'))


df = pd.read_csv("https://api.kite.trade/instruments")
nse_filter = (df['exchange'] == "NSE") & (df['name'].notnull()) & (df['segment'] != "INDICES") & (~df["tradingsymbol"].str.contains("-BE"))
df_nse = df[nse_filter]
df_nse_list = df[nse_filter]['tradingsymbol'].values.tolist()
length = len(df_nse_list)
#@st.cache(suppress_st_warning=True)
def df_values():
    change_percentage_keys_list = []
    volume_keys_list = []
    ltp_keys_list = []
    vwap_keys_list = []

    for i in range(length):    
        change_percentage_keys_list.append(df_nse_list[i]+'_CHANGE_PERCENTAGE')
        volume_keys_list.append(df_nse_list[i]+'_VOLUME')
        ltp_keys_list.append(df_nse_list[i]+'_LTP')
        vwap_keys_list.append(df_nse_list[i]+'_VWAP')
        
        
    #print(change_percentage_keys_list)
    percentage_value =  r.mget(change_percentage_keys_list)
    volume_value =   r.mget(volume_keys_list)
    ltp_value =   r.mget(ltp_keys_list)
    vwap_value =   r.mget(vwap_keys_list)

    df = pd.DataFrame(list(zip(df_nse_list, ltp_value, percentage_value, volume_value,vwap_value)),    columns=['ticker','ltp','change_percentage', 'volume','vwap'])
    df = df.fillna(0)
    #print(df)
    #===column conversion
    df['ltp'] = df['ltp'].astype(float)
    df['volume'] = df['volume'].astype(int)
    df['change_percentage'] = df['change_percentage'].astype(float)
    df['contract_value'] = (df['volume'] * df['ltp']).astype(int)
    df['vwap'] = df['vwap'].astype(float)
    df['vwap_diff'] = df['ltp'] -  df['vwap']
    return df

#streamlit column 
c1,c2,c3 = st.columns(3)
c1.header("Upper Circuit Leaders")
c1_df = c1.empty()


#===higher circuit

def upper_circuit_leaders_df():
    
    global tmp_df
    tmp_df = tmp_df.sort_values(by='change_percentage', ascending=False)
    tmp_df.reset_index(drop=True, inplace=True)
    #st.write("Upper Circuit")
    c1_df.dataframe(tmp_df)
    #c1_df.dataframe(tmp_df.style.highlight_max(axis=0)) #https://docs.streamlit.io/library/api-reference/data/st.dataframe
# c = alt.Chart(df.head(500)).mark_circle().encode( x='ticker', y='change_percentage', size='volume', color='volume', tooltip=['ticker', 'change_percentage', 'volume'])
# st.altair_chart(c, use_container_width=False)
#===lower circuit
c2.header("Lower Circuit Leaders")
c2_df = c2.empty()

def lower_circuit_leaders_df():
    global tmp_df
    tmp_df = tmp_df.sort_values(by='change_percentage', ascending=True)
    tmp_df.reset_index(drop=True, inplace=True)
    #st.write("Lower Circuit")
    c2_df.dataframe(tmp_df)

#===VOLUME high to low
c3.header("Volume Leaders")
c3_df = c3.empty()
def volume_leaders_df():
    global tmp_df
    tmp_df = tmp_df.sort_values(by='volume', ascending=False)
    tmp_df.reset_index(drop=True, inplace=True)
    c3_df.dataframe(tmp_df)

#=========
c4,c5,c6 = st.columns(3)
#volume value gainers
c4.header("Volume Gain Leaders")
c4_df = c4.empty()
def volume_gain_leaders_df():
    global tmp_df
    v_gain_filter = (tmp_df['change_percentage'] >= 0)
    df_v_gain = tmp_df[v_gain_filter]
    #df_v_gain = df_v_gain.head(30) 
    df_v_gain = df_v_gain.sort_values(by='volume', ascending=False) #volume change_percentage
    df_v_gain.reset_index(drop=True, inplace=True)
    c4_df.dataframe(df_v_gain)
#volume value loss
c5.header("Volume Loss Leaders")
c5_df = c5.empty()
def volume_loss_leaders_df():
    global tmp_df
    v_loss_filter = (tmp_df['change_percentage'] <= 0)
    df_v_loss = tmp_df[v_loss_filter]
    #df_v_loss = df_v_loss.head(30) # taking only top 
    df_v_loss = df_v_loss.sort_values(by='volume', ascending=True) #volume change_percentage
    df_v_loss.reset_index(drop=True, inplace=True)
    c5_df.dataframe(df_v_loss)
#===========
#===Contract Value high to low
c6.header("Contract Value Leaders")
c6_df = c6.empty()
def contract_value_leaders_df():
    global tmp_df
    #tmp_df = df_values()
    df = tmp_df.sort_values(by='contract_value', ascending=False)
    df.reset_index(drop=True, inplace=True)
    c6_df.dataframe(df)

c7,c8,c9 = st.columns(3)
#contract value gainers
c7.header("CV Gain Leader")
c7_df = c7.empty()
def contract_gain_leaders_df():
    global tmp_df
    cv_gain_filter = (tmp_df['change_percentage'] >= 0)
    df_cv_gain = tmp_df[cv_gain_filter]
    #df_cv_gain = df_cv_gain.head(30) # taking only top 
    #df_cv_gain = df_cv_gain.sort_values(by='change_percentage', ascending=False) #contract_value
    df_cv_gain = df_cv_gain.sort_values(by='contract_value', ascending=False) #contract_value
    df_cv_gain.reset_index(drop=True, inplace=True)
    c7_df.dataframe(df_cv_gain)
#contract value loss
c8.header("CV Loss Leaders")
c8_df = c8.empty()
def contract_loss_leaders_df():
    global tmp_df
    cv_loss_filter = (tmp_df['change_percentage'] <= 0)
    df_cv_loss = tmp_df[cv_loss_filter]
    #df_cv_loss = df_cv_loss.head(30) # taking only top 
    #df_cv_loss = df_cv_loss.sort_values(by='change_percentage', ascending=True) #contract_value
    df_cv_loss = df_cv_loss.sort_values(by='contract_value', ascending=False) #contract_value
    df_cv_loss.reset_index(drop=True, inplace=True)
    c8_df.dataframe(df_cv_loss)

#==================definedge_scanner
c9.header("Definedge Breakout Scan")
c9_df = c9.empty()
breakout_definedge_csv_df = pd.read_csv("definedge_csv/nifty_fusion_all.csv") 
#@st.cache(suppress_st_warning=True)
def definedge_breakout_csv_df():
    # df = pd.read_csv("/home/ubuntu/definedge/csv/fusion_nifty_all.csv") 
    df = breakout_definedge_csv_df
    #df = df[['Scrip','DTB Level','DBS Level']]
    #print(df)
    c9_df.dataframe(df) 
    return df

definedge_breakout_csv_df()

c10,c11 = st.columns(2)
c10.header("Definedge Buy Triggered")
c10_df = c10.empty()
c11.header("Definedge Sell Triggered")
c11_df = c11.empty()
#buy and sell count column
c12,c13 = st.columns(2)
tmp_c12 = c12.empty()
tmp_c13 = c13.empty()

def definedge_breakout_csv_scan_df():
    #df = definedge_breakout_csv_df()
    df = breakout_definedge_csv_df
    buy_count = 0
    sell_count = 0

    buy_ticker = []
    buy_ltp = []
    buy_change_percentage = []
    buy_volume = []

    sell_ticker = []
    sell_ltp = []
    sell_change_percentage = []
    sell_volume = []



    for index, row in df.iterrows():
        buy_level = row['DTB Level']
        sell_level = row['DBS Level']
        ticker = row['Scrip']
        ltp = r.get(row['Scrip']+"_LTP")
        cp = r.get(row['Scrip']+"_CHANGE_PERCENTAGE")
        vol = r.get(row['Scrip']+"_VOLUME")

        if( isinstance(ltp, type(None))):
            continue
        ltp_float = float(ltp)
        if(sell_level!=0 and ltp_float<sell_level):
            sell_count+=1
            #list data append
            sell_ticker.append(ticker)
            sell_ltp.append(ltp)
            sell_change_percentage.append(cp)
            sell_volume.append(vol)
            #print("sell triggered : "+ticker)
        if(buy_level!=0 and ltp_float>buy_level):
            buy_count+=1 
            #list data append
            buy_ticker.append(ticker)
            buy_ltp.append(ltp)
            buy_change_percentage.append(cp)
            buy_volume.append(vol)
            #print("buy triggered : "+ticker)
        #print(row['Scrip'] + " : "+str(ltp_float))



    buy_df = pd.DataFrame(list(zip(buy_ticker, buy_ltp, buy_change_percentage,buy_volume)),    columns=['ticker','ltp','change_percentage', 'volume'])
    #===data type conversion
    buy_df['ltp'] = buy_df['ltp'].astype(float)
    buy_df['volume'] = buy_df['volume'].astype(int)
    buy_df['change_percentage'] = buy_df['change_percentage'].astype(float)
    buy_df = buy_df.sort_values(by='change_percentage', ascending=False)
    buy_df['contract_value'] = (buy_df['volume'] * buy_df['ltp']).astype(int)
    buy_df.reset_index(drop=True, inplace=True)
    #print(buy_df)
  
    tmp_c12.write("buy_count : "+str(buy_count))
    c10_df.dataframe(buy_df)
    #print("=================")
    #print("buy_count : "+str(buy_count))

    #print("=================")

    sell_df = pd.DataFrame(list(zip(sell_ticker, sell_ltp, sell_change_percentage,sell_volume)),    columns=['ticker','ltp','change_percentage', 'volume'])
    #===data type conversion
    sell_df['ltp'] = sell_df['ltp'].astype(float)
    sell_df['volume'] = sell_df['volume'].astype(int)
    sell_df['change_percentage'] = sell_df['change_percentage'].astype(float)
    sell_df = sell_df.sort_values(by='change_percentage', ascending=False)
    sell_df['contract_value'] = (sell_df['volume'] * sell_df['ltp']).astype(int)
    sell_df.reset_index(drop=True, inplace=True)
    #print(sell_df)
    
    tmp_c13.write("sell_count : "+str(sell_count))
    c11_df.dataframe(sell_df)
    #print("=================")
    #print("sell_count : "+str(sell_count))

    #print("=================")

#==============================
b1,b2 = st.columns(2)
b1.header("Trending oi -  BANKNIFTY - CE")
b1_df = b1.empty()
b2.header("Trending oi -  BANKNIFTY - PE")
b2_df = b2.empty()
#bnf ce and pe column
bnf_ce_oi_total,bnf_pe_oi_total = st.columns(2)
tmp_bnf_ce_oi_total = bnf_ce_oi_total.empty()
tmp_bnf_pe_oi_total = bnf_pe_oi_total.empty()
#@st.cache(suppress_st_warning=True)
def bnf_ce():
    #banknifty_ce oi ============
    banknifty_ce_oi_keys_list = r.keys("BANKNIFTY*CE_OI")
    banknifty_ce_oi_values =  r.mget(banknifty_ce_oi_keys_list)

    banknifty_ce_ticker_list = []
    banknifty_ce_strike_list = []
    banknifty_ce_strike_type_list = []
    banknifty_ce_volume = []
    banknifty_ce_ltp = []
    for i in banknifty_ce_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")

        banknifty_ce_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        banknifty_ce_volume.append(ticker_volume)

        banknifty_ce_ticker_list.append(ticker_name)
        banknifty_ce_strike_list.append(tmp_str[-10:-5])
        banknifty_ce_strike_type_list.append(tmp_str[-5:-3])
    banknifty_ce_oi_df = pd.DataFrame(list(zip(banknifty_ce_ticker_list,banknifty_ce_oi_values,banknifty_ce_strike_list,banknifty_ce_strike_type_list,banknifty_ce_ltp,banknifty_ce_volume)),columns=["banknifty_ce_ticker","banknifty_ce_oi",'banknifty_ce_strike','banknifty_ce_strike_type','banknifty_ce_ltp','banknifty_ce_volume'])
    banknifty_ce_oi_df = banknifty_ce_oi_df.fillna(0)
    banknifty_ce_oi_df['banknifty_ce_ltp'] = banknifty_ce_oi_df['banknifty_ce_ltp'].astype(float)
    banknifty_ce_oi_df['banknifty_ce_volume'] = banknifty_ce_oi_df['banknifty_ce_volume'].astype(int)
    banknifty_ce_oi_df['banknifty_ce_contract_value'] = (banknifty_ce_oi_df['banknifty_ce_volume'] * banknifty_ce_oi_df['banknifty_ce_ltp']).astype(int)

    banknifty_ce_oi_df['banknifty_ce_oi'] = banknifty_ce_oi_df['banknifty_ce_oi'].astype(float)
    banknifty_ce_oi_df['banknifty_ce_ticker'] = banknifty_ce_oi_df['banknifty_ce_ticker'].astype(str)
    banknifty_ce_oi_df = banknifty_ce_oi_df.sort_values(by='banknifty_ce_contract_value', ascending=False)
    banknifty_ce_oi_df.reset_index(drop=True, inplace=True)
    b1_df.dataframe(banknifty_ce_oi_df)
    banknifty_ce_oi_total = banknifty_ce_oi_df['banknifty_ce_oi'].sum()
    tmp_bnf_ce_oi_total.write("banknifty_ce_oi_total : "+str(banknifty_ce_oi_total))
    return banknifty_ce_oi_df,banknifty_ce_oi_total
#banknifty_pe oi ============
#@st.cache(suppress_st_warning=True)
def bnf_pe():
    banknifty_pe_oi_keys_list = r.keys("BANKNIFTY*PE_OI")
    banknifty_pe_oi_values =  r.mget(banknifty_pe_oi_keys_list)

    banknifty_pe_ticker_list = []
    banknifty_pe_strike_list = []
    banknifty_pe_strike_type_list = []
    banknifty_pe_volume = []
    banknifty_pe_ltp = []
    for i in banknifty_pe_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        banknifty_pe_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        banknifty_pe_volume.append(ticker_volume)

        banknifty_pe_ticker_list.append(ticker_name)
        banknifty_pe_strike_list.append(tmp_str[-10:-5])
        banknifty_pe_strike_type_list.append(tmp_str[-5:-3])
    banknifty_pe_oi_df = pd.DataFrame(list(zip(banknifty_pe_ticker_list,banknifty_pe_oi_values,banknifty_pe_strike_list,banknifty_pe_strike_type_list,banknifty_pe_ltp,banknifty_pe_volume)),columns=["banknifty_pe_ticker","banknifty_pe_oi",'banknifty_pe_strike','banknifty_pe_strike_type','banknifty_pe_ltp','banknifty_pe_volume'])
    banknifty_pe_oi_df = banknifty_pe_oi_df.fillna(0)
    banknifty_pe_oi_df['banknifty_pe_ltp'] = banknifty_pe_oi_df['banknifty_pe_ltp'].astype(float)
    banknifty_pe_oi_df['banknifty_pe_volume'] = banknifty_pe_oi_df['banknifty_pe_volume'].astype(int)
    banknifty_pe_oi_df['banknifty_pe_contract_value'] = (banknifty_pe_oi_df['banknifty_pe_volume'] * banknifty_pe_oi_df['banknifty_pe_ltp']).astype(int)

    banknifty_pe_oi_df['banknifty_pe_oi'] = banknifty_pe_oi_df['banknifty_pe_oi'].astype(float)
    banknifty_pe_oi_df['banknifty_pe_ticker'] = banknifty_pe_oi_df['banknifty_pe_ticker'].astype(str)
    banknifty_pe_oi_df = banknifty_pe_oi_df.sort_values(by='banknifty_pe_contract_value', ascending=False)
    banknifty_pe_oi_df.reset_index(drop=True, inplace=True)
    b2_df.dataframe(banknifty_pe_oi_df)
    banknifty_pe_oi_total = banknifty_pe_oi_df['banknifty_pe_oi'].sum()
    tmp_bnf_pe_oi_total.write("banknifty_pe_oi_total : "+str(banknifty_pe_oi_total))
    return banknifty_pe_oi_df, banknifty_pe_oi_total

b3,b4 = st.columns(2)
b3_df = b3.empty()
b4_df = b4.empty() 
def bnf_suggestion():   
    #b3 = st.empty()
    banknifty_pe_oi_df,banknifty_pe_oi_total =  bnf_pe()
    banknifty_ce_oi_df,banknifty_ce_oi_total =  bnf_ce()
    #b3_df.write("banknifty_pe_oi_total : "+str(banknifty_pe_oi_total))
    banknifty_trending_oi_total = banknifty_pe_oi_total - banknifty_ce_oi_total
    if(banknifty_trending_oi_total<0):
        banknifty_trending_oi_suggestion ="<font color='red'>Bearish</font>"
    else:
        banknifty_trending_oi_suggestion = "<font color='green'>Bullish</font>"       
    b3_df.write("Banknifty trending_oi_total : "+str(banknifty_trending_oi_total))
    b4_df.markdown("Banknifty trending_oi_suggestion : "+banknifty_trending_oi_suggestion, unsafe_allow_html=True)



#============all strike  - banknifty oi 
#b5 = st.columns(1)
b5 = st.empty()
b5.header("BANKNIFTY - OI - CHAIN")
#b5_df = b5.empty()
b5_oc = st.empty()

# b5_df = st.empty()
# b5_df.header("BANKNIFTY - OI - CHAIN")

def bnf_oi_chain(): 
    banknifty_pe_oi_df,banknifty_pe_oi_total =  bnf_pe()
    banknifty_ce_oi_df,banknifty_ce_oi_total =  bnf_ce()
    banknifty_all_strike_oi_df = pd.merge(banknifty_pe_oi_df, banknifty_ce_oi_df,left_on='banknifty_pe_strike',right_on='banknifty_ce_strike') #https://realpython.com/pandas-merge-join-and-concat/
    banknifty_all_strike_oi_df = banknifty_all_strike_oi_df.sort_values(by='banknifty_ce_strike', ascending=True)
    #TICKER INFORMATION pe_ticker and ce_ticker below - blocked for space contract  -working
    #banknifty_all_strike_oi_df = banknifty_all_strike_oi_df [['banknifty_pe_ticker','banknifty_pe_oi','banknifty_pe_contract_value','banknifty_pe_volume','banknifty_pe_ltp','banknifty_pe_strike','banknifty_ce_ltp','banknifty_ce_volume','banknifty_ce_contract_value','banknifty_ce_oi','banknifty_ce_ticker']]
    banknifty_all_strike_oi_df = banknifty_all_strike_oi_df [['banknifty_pe_oi','banknifty_pe_contract_value','banknifty_pe_volume','banknifty_pe_ltp','banknifty_pe_strike','banknifty_ce_ltp','banknifty_ce_volume','banknifty_ce_contract_value','banknifty_ce_oi']]
    banknifty_all_strike_oi_df = banknifty_all_strike_oi_df.rename(columns={'banknifty_pe_strike': 'strike'})
    banknifty_all_strike_oi_df = banknifty_all_strike_oi_df.sort_values(by='strike', ascending=False)

    b5_oc.dataframe(banknifty_all_strike_oi_df)

#========chart - bnf
b6,b7 = st.columns(2)
b6_df = b6.empty()
b7_df = b7.empty()
def bnf_oi_chain_chart():
    #bnf
    banknifty_pe_oi_df,banknifty_pe_oi_total =  bnf_pe()
    banknifty_ce_oi_df,banknifty_ce_oi_total =  bnf_ce()
    bnf_ce1 = banknifty_ce_oi_df[['banknifty_ce_strike','banknifty_ce_ltp','banknifty_ce_volume','banknifty_ce_contract_value','banknifty_ce_oi','banknifty_ce_strike_type']]
    bnf_ce1 = bnf_ce1.rename(columns={'banknifty_ce_strike': 'strike','banknifty_ce_ltp':'ltp','banknifty_ce_volume':'volume','banknifty_ce_contract_value':'contract_value','banknifty_ce_oi':'oi','banknifty_ce_strike_type':'option_type'})
    #st.dataframe(bnf_ce)

    bnf_pe1 = banknifty_pe_oi_df[['banknifty_pe_strike','banknifty_pe_ltp','banknifty_pe_volume','banknifty_pe_contract_value','banknifty_pe_oi','banknifty_pe_strike_type']]
    bnf_pe1 = bnf_pe1.rename(columns={'banknifty_pe_strike': 'strike','banknifty_pe_ltp':'ltp','banknifty_pe_volume':'volume','banknifty_pe_contract_value':'contract_value','banknifty_pe_oi':'oi','banknifty_pe_strike_type':'option_type'})
    #st.dataframe(bnf_pe)

    bnf_stacked_df_all = pd.concat([bnf_ce1,bnf_pe1])
    bnf_stacked_df_all = bnf_stacked_df_all.sort_values(by='strike', ascending=False)
    bnf_stacked_df_all.reset_index(drop=True, inplace=True)
    #st.dataframe(bnf_stacked_df_all)

    #https://vega.github.io/vega-lite/examples/bar_grouped.html
    #oi
    b6_df.vega_lite_chart(bnf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'oi', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

    #ltp
    b7_df.vega_lite_chart(bnf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'ltp', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

#==========NIFTY
#=======================
n1,n2 = st.columns(2)
n3,n4 = st.columns(2)
tn1,tn2 = st.columns(2)
tn1_df = tn1.empty()
tn2_df = tn2.empty()
n1.header("Trending oi -  NIFTY - CE")
#nifty_ce oi ============
n1_df = n1.empty()
n3_df = n3.empty()
#@st.cache(suppress_st_warning=True)
def nifty_ce():
    nifty_ce_oi_keys_list = r.keys("NIFTY*CE_OI")
    nifty_ce_oi_values =  r.mget(nifty_ce_oi_keys_list)

    nifty_ce_ticker_list = []
    nifty_ce_strike_list = []
    nifty_ce_strike_type_list = []
    nifty_ce_volume = []
    nifty_ce_ltp = []
    for i in nifty_ce_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        nifty_ce_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        nifty_ce_volume.append(ticker_volume)

        nifty_ce_ticker_list.append(ticker_name)
        nifty_ce_strike_list.append(tmp_str[-10:-5])
        nifty_ce_strike_type_list.append(tmp_str[-5:-3])
    nifty_ce_oi_df = pd.DataFrame(list(zip(nifty_ce_ticker_list,nifty_ce_oi_values,nifty_ce_strike_list,nifty_ce_strike_type_list,nifty_ce_ltp,nifty_ce_volume)),columns=["nifty_ce_ticker","nifty_ce_oi",'nifty_ce_strike','nifty_ce_strike_type','nifty_ce_ltp','nifty_ce_volume'])
    nifty_ce_oi_df = nifty_ce_oi_df.fillna(0)
    nifty_ce_oi_df['nifty_ce_ltp'] = nifty_ce_oi_df['nifty_ce_ltp'].astype(float)
    nifty_ce_oi_df['nifty_ce_volume'] = nifty_ce_oi_df['nifty_ce_volume'].astype(int)
    nifty_ce_oi_df['nifty_ce_contract_value'] = (nifty_ce_oi_df['nifty_ce_volume'] * nifty_ce_oi_df['nifty_ce_ltp']).astype(int)

    nifty_ce_oi_df['nifty_ce_oi'] = nifty_ce_oi_df['nifty_ce_oi'].astype(float)
    nifty_ce_oi_df['nifty_ce_ticker'] = nifty_ce_oi_df['nifty_ce_ticker'].astype(str)
    nifty_ce_oi_df = nifty_ce_oi_df.sort_values(by='nifty_ce_contract_value', ascending=False)
    nifty_ce_oi_df.reset_index(drop=True, inplace=True)
    n1_df.dataframe(nifty_ce_oi_df)
    nifty_ce_oi_total = nifty_ce_oi_df['nifty_ce_oi'].sum()
    n3_df.write("nifty_ce_oi_total : "+str(nifty_ce_oi_total))
    return nifty_ce_oi_df,nifty_ce_oi_total
#nifty_pe oi ============
n2.header("Trending oi -  NIFTY - PE")
n2_df = n2.empty()
n4_df = n4.empty()
#@st.cache(suppress_st_warning=True)
def nifty_pe():
    nifty_ce_oi_df,nifty_ce_oi_total = nifty_ce()
    nifty_pe_oi_keys_list = r.keys("NIFTY*PE_OI")
    nifty_pe_oi_values =  r.mget(nifty_pe_oi_keys_list)

    nifty_pe_ticker_list = []
    nifty_pe_strike_list = []
    nifty_pe_strike_type_list = []
    nifty_pe_volume = []
    nifty_pe_ltp = []
    for i in nifty_pe_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        nifty_pe_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        nifty_pe_volume.append(ticker_volume)

        nifty_pe_ticker_list.append(ticker_name)
        nifty_pe_strike_list.append(tmp_str[-10:-5])
        nifty_pe_strike_type_list.append(tmp_str[-5:-3])
    nifty_pe_oi_df = pd.DataFrame(list(zip(nifty_pe_ticker_list,nifty_pe_oi_values,nifty_pe_strike_list,nifty_pe_strike_type_list,nifty_pe_ltp,nifty_pe_volume)),columns=["nifty_pe_ticker","nifty_pe_oi",'nifty_pe_strike','nifty_pe_strike_type','nifty_pe_ltp','nifty_pe_volume'])
    nifty_pe_oi_df = nifty_pe_oi_df.fillna(0)
    nifty_pe_oi_df['nifty_pe_ltp'] = nifty_pe_oi_df['nifty_pe_ltp'].astype(float)
    nifty_pe_oi_df['nifty_pe_volume'] = nifty_pe_oi_df['nifty_pe_volume'].astype(int)
    nifty_pe_oi_df['nifty_pe_contract_value'] = (nifty_pe_oi_df['nifty_pe_volume'] * nifty_pe_oi_df['nifty_pe_ltp']).astype(int)

    nifty_pe_oi_df['nifty_pe_oi'] = nifty_pe_oi_df['nifty_pe_oi'].astype(float)
    nifty_pe_oi_df['nifty_pe_ticker'] = nifty_pe_oi_df['nifty_pe_ticker'].astype(str)
    nifty_pe_oi_df = nifty_pe_oi_df.sort_values(by='nifty_pe_contract_value', ascending=False)
    nifty_pe_oi_df.reset_index(drop=True, inplace=True)
    n2_df.dataframe(nifty_pe_oi_df)
    nifty_pe_oi_total = nifty_pe_oi_df['nifty_pe_oi'].sum()
    n4_df.write("nifty_pe_oi_total : "+str(nifty_pe_oi_total))
    nifty_trending_oi_total = nifty_pe_oi_total - nifty_ce_oi_total
    if(nifty_trending_oi_total<0):
        nifty_trending_oi_suggestion = "<font color='red'>Bearish</font>"
    else:
        nifty_trending_oi_suggestion =  "<font color='green'>Bullish</font>"
    tn1_df.write("Nifty trending_oi_total : "+str(nifty_trending_oi_total))
    tn2_df.markdown("Nifty trending_oi_suggestion : "+nifty_trending_oi_suggestion, unsafe_allow_html=True)
    return nifty_pe_oi_df,nifty_pe_oi_total
#============all strike  - nifty oi 
n5 = st.empty()
n5.header("NIFTY - OI - CHAIN")
n5_df = st.empty()

n6,n7 = st.columns(2)
n6_df = n6.empty()
n7_df = n7.empty()
def nf_oi_chain_chart():
    nifty_ce_oi_df,nifty_ce_oi_total = nifty_ce()
    nifty_pe_oi_df,nifty_pe_oi_total = nifty_pe()
    #st.header("NIFTY - OI - CHAIN")
    nifty_all_strike_oi_df = pd.merge(nifty_pe_oi_df, nifty_ce_oi_df,left_on='nifty_pe_strike',right_on='nifty_ce_strike') #https://realpython.com/pandas-merge-join-and-concat/
    nifty_all_strike_oi_df = nifty_all_strike_oi_df.sort_values(by='nifty_ce_strike', ascending=True)
    #TICKER INFORMATION pe_ticker and ce_ticker below - blocked for space contract  -working
    #nifty_all_strike_oi_df = nifty_all_strike_oi_df [['nifty_pe_ticker','nifty_pe_oi','nifty_pe_contract_value','nifty_pe_volume','nifty_pe_ltp','nifty_pe_strike','nifty_ce_ltp','nifty_ce_volume','nifty_ce_contract_value','nifty_ce_oi','nifty_ce_ticker']]
    nifty_all_strike_oi_df = nifty_all_strike_oi_df [['nifty_pe_oi','nifty_pe_contract_value','nifty_pe_volume','nifty_pe_ltp','nifty_pe_strike','nifty_ce_ltp','nifty_ce_volume','nifty_ce_contract_value','nifty_ce_oi']]
    nifty_all_strike_oi_df = nifty_all_strike_oi_df.rename(columns={'nifty_pe_strike': 'strike'})
    nifty_all_strike_oi_df = nifty_all_strike_oi_df.sort_values(by='strike', ascending=False)
    n5_df.dataframe(nifty_all_strike_oi_df)


    #========chart - nf
    #nf
    nf_ce = nifty_ce_oi_df[['nifty_ce_strike','nifty_ce_ltp','nifty_ce_volume','nifty_ce_contract_value','nifty_ce_oi','nifty_ce_strike_type']]
    nf_ce = nf_ce.rename(columns={'nifty_ce_strike': 'strike','nifty_ce_ltp':'ltp','nifty_ce_volume':'volume','nifty_ce_contract_value':'contract_value','nifty_ce_oi':'oi','nifty_ce_strike_type':'option_type'})

    nf_pe = nifty_pe_oi_df[['nifty_pe_strike','nifty_pe_ltp','nifty_pe_volume','nifty_pe_contract_value','nifty_pe_oi','nifty_pe_strike_type']]
    nf_pe = nf_pe.rename(columns={'nifty_pe_strike': 'strike','nifty_pe_ltp':'ltp','nifty_pe_volume':'volume','nifty_pe_contract_value':'contract_value','nifty_pe_oi':'oi','nifty_pe_strike_type':'option_type'})

    nf_stacked_df_all = pd.concat([nf_ce,nf_pe])
    nf_stacked_df_all = nf_stacked_df_all.sort_values(by='strike', ascending=False)
    nf_stacked_df_all.reset_index(drop=True, inplace=True)


    #https://vega.github.io/vega-lite/examples/bar_grouped.html
    #oi
    n6_df.vega_lite_chart(nf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'oi', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

    #ltp
    n7_df.vega_lite_chart(nf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'ltp', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

#===finnifty
#==========FINNIFTY
#==========FINNIFTY
#=======================
f1,f2 = st.columns(2)
f3,f4 = st.columns(2)
tf1,tf2 = st.columns(2)
tf1_df = tf1.empty()
tf2_df = tf2.empty()
f1.header("Trending oi -  FINNIFTY - CE")
#finnifty_ce oi ============
f1_df = f1.empty()
f3_df = f3.empty()
#@st.cache(suppress_st_warning=True)
def finnifty_ce():
    finnifty_ce_oi_keys_list = r.keys("FINNIFTY*CE_OI")
    finnifty_ce_oi_values =  r.mget(finnifty_ce_oi_keys_list)

    finnifty_ce_ticker_list = []
    finnifty_ce_strike_list = []
    finnifty_ce_strike_type_list = []
    finnifty_ce_volume = []
    finnifty_ce_ltp = []
    for i in finnifty_ce_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        finnifty_ce_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        finnifty_ce_volume.append(ticker_volume)

        finnifty_ce_ticker_list.append(ticker_name)
        finnifty_ce_strike_list.append(tmp_str[-10:-5])
        finnifty_ce_strike_type_list.append(tmp_str[-5:-3])
    finnifty_ce_oi_df = pd.DataFrame(list(zip(finnifty_ce_ticker_list,finnifty_ce_oi_values,finnifty_ce_strike_list,finnifty_ce_strike_type_list,finnifty_ce_ltp,finnifty_ce_volume)),columns=["finnifty_ce_ticker","finnifty_ce_oi",'finnifty_ce_strike','finnifty_ce_strike_type','finnifty_ce_ltp','finnifty_ce_volume'])
    finnifty_ce_oi_df = finnifty_ce_oi_df.fillna(0)
    finnifty_ce_oi_df['finnifty_ce_ltp'] = finnifty_ce_oi_df['finnifty_ce_ltp'].astype(float)
    finnifty_ce_oi_df['finnifty_ce_volume'] = finnifty_ce_oi_df['finnifty_ce_volume'].astype(int)
    finnifty_ce_oi_df['finnifty_ce_contract_value'] = (finnifty_ce_oi_df['finnifty_ce_volume'] * finnifty_ce_oi_df['finnifty_ce_ltp']).astype(int)

    finnifty_ce_oi_df['finnifty_ce_oi'] = finnifty_ce_oi_df['finnifty_ce_oi'].astype(float)
    finnifty_ce_oi_df['finnifty_ce_ticker'] = finnifty_ce_oi_df['finnifty_ce_ticker'].astype(str)
    finnifty_ce_oi_df = finnifty_ce_oi_df.sort_values(by='finnifty_ce_contract_value', ascending=False)
    finnifty_ce_oi_df.reset_index(drop=True, inplace=True)
    f1_df.dataframe(finnifty_ce_oi_df)
    finnifty_ce_oi_total = finnifty_ce_oi_df['finnifty_ce_oi'].sum()
    f3_df.write("finnifty_ce_oi_total : "+str(finnifty_ce_oi_total))
    return finnifty_ce_oi_df,finnifty_ce_oi_total
#finnifty_pe oi ============
f2.header("Trending oi -  FINNIFTY - PE")
f2_df = f2.empty()
f4_df = f4.empty()
#@st.cache(suppress_st_warning=True)
def finnifty_pe():
    finnifty_ce_oi_df,finnifty_ce_oi_total = finnifty_ce()
    finnifty_pe_oi_keys_list = r.keys("FINNIFTY*PE_OI")
    finnifty_pe_oi_values =  r.mget(finnifty_pe_oi_keys_list)

    finnifty_pe_ticker_list = []
    finnifty_pe_strike_list = []
    finnifty_pe_strike_type_list = []
    finnifty_pe_volume = []
    finnifty_pe_ltp = []
    for i in finnifty_pe_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        finnifty_pe_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        finnifty_pe_volume.append(ticker_volume)

        finnifty_pe_ticker_list.append(ticker_name)
        finnifty_pe_strike_list.append(tmp_str[-10:-5])
        finnifty_pe_strike_type_list.append(tmp_str[-5:-3])
    finnifty_pe_oi_df = pd.DataFrame(list(zip(finnifty_pe_ticker_list,finnifty_pe_oi_values,finnifty_pe_strike_list,finnifty_pe_strike_type_list,finnifty_pe_ltp,finnifty_pe_volume)),columns=["finnifty_pe_ticker","finnifty_pe_oi",'finnifty_pe_strike','finnifty_pe_strike_type','finnifty_pe_ltp','finnifty_pe_volume'])
    finnifty_pe_oi_df = finnifty_pe_oi_df.fillna(0)
    finnifty_pe_oi_df['finnifty_pe_ltp'] = finnifty_pe_oi_df['finnifty_pe_ltp'].astype(float)
    finnifty_pe_oi_df['finnifty_pe_volume'] = finnifty_pe_oi_df['finnifty_pe_volume'].astype(int)
    finnifty_pe_oi_df['finnifty_pe_contract_value'] = (finnifty_pe_oi_df['finnifty_pe_volume'] * finnifty_pe_oi_df['finnifty_pe_ltp']).astype(int)

    finnifty_pe_oi_df['finnifty_pe_oi'] = finnifty_pe_oi_df['finnifty_pe_oi'].astype(float)
    finnifty_pe_oi_df['finnifty_pe_ticker'] = finnifty_pe_oi_df['finnifty_pe_ticker'].astype(str)
    finnifty_pe_oi_df = finnifty_pe_oi_df.sort_values(by='finnifty_pe_contract_value', ascending=False)
    finnifty_pe_oi_df.reset_index(drop=True, inplace=True)
    f2_df.dataframe(finnifty_pe_oi_df)
    finnifty_pe_oi_total = finnifty_pe_oi_df['finnifty_pe_oi'].sum()
    f4_df.write("finnifty_pe_oi_total : "+str(finnifty_pe_oi_total))
    nifty_trending_oi_total = finnifty_pe_oi_total - finnifty_ce_oi_total
    if(nifty_trending_oi_total<0):
        nifty_trending_oi_suggestion = "<font color='red'>Bearish</font>"
    else:
        nifty_trending_oi_suggestion =  "<font color='green'>Bullish</font>"
    tf1_df.write("FinNifty trending_oi_total : "+str(nifty_trending_oi_total))
    tf2_df.markdown("FinNifty trending_oi_suggestion : "+nifty_trending_oi_suggestion, unsafe_allow_html=True)
    return finnifty_pe_oi_df,finnifty_pe_oi_total
#============all strike  - FinNifty oi 
f5 = st.empty()
f5.header("FINNIFTY - OI - CHAIN")
f5_df = st.empty()

f6,f7 = st.columns(2)
f6_df = f6.empty()
f7_df = f7.empty()
def fnf_oi_chain_chart():
    finnifty_ce_oi_df,finnifty_ce_oi_total = finnifty_ce()
    finnifty_pe_oi_df,finnifty_pe_oi_total = finnifty_pe()
    #st.header("FINNIFTY - OI - CHAIN")
    finnifty_all_strike_oi_df = pd.merge(finnifty_pe_oi_df, finnifty_ce_oi_df,left_on='finnifty_pe_strike',right_on='finnifty_ce_strike') #https://realpython.com/pandas-merge-join-and-concat/
    finnifty_all_strike_oi_df = finnifty_all_strike_oi_df.sort_values(by='finnifty_ce_strike', ascending=True)
    #TICKER INFORMATION pe_ticker and ce_ticker below - blocked for space contract  -working
    #finnifty_all_strike_oi_df = finnifty_all_strike_oi_df [['finnifty_pe_ticker','finnifty_pe_oi','finnifty_pe_contract_value','finnifty_pe_volume','finnifty_pe_ltp','finnifty_pe_strike','finnifty_ce_ltp','finnifty_ce_volume','finnifty_ce_contract_value','finnifty_ce_oi','finnifty_ce_ticker']]
    finnifty_all_strike_oi_df = finnifty_all_strike_oi_df [['finnifty_pe_oi','finnifty_pe_contract_value','finnifty_pe_volume','finnifty_pe_ltp','finnifty_pe_strike','finnifty_ce_ltp','finnifty_ce_volume','finnifty_ce_contract_value','finnifty_ce_oi']]
    finnifty_all_strike_oi_df = finnifty_all_strike_oi_df.rename(columns={'finnifty_pe_strike': 'strike'})
    finnifty_all_strike_oi_df = finnifty_all_strike_oi_df.sort_values(by='strike', ascending=False)
    f5_df.dataframe(finnifty_all_strike_oi_df)


    #========chart - nf
    #nf
    fnf_ce = finnifty_ce_oi_df[['finnifty_ce_strike','finnifty_ce_ltp','finnifty_ce_volume','finnifty_ce_contract_value','finnifty_ce_oi','finnifty_ce_strike_type']]
    fnf_ce = fnf_ce.rename(columns={'finnifty_ce_strike': 'strike','finnifty_ce_ltp':'ltp','finnifty_ce_volume':'volume','finnifty_ce_contract_value':'contract_value','finnifty_ce_oi':'oi','finnifty_ce_strike_type':'option_type'})

    fnf_pe = finnifty_pe_oi_df[['finnifty_pe_strike','finnifty_pe_ltp','finnifty_pe_volume','finnifty_pe_contract_value','finnifty_pe_oi','finnifty_pe_strike_type']]
    fnf_pe = fnf_pe.rename(columns={'finnifty_pe_strike': 'strike','finnifty_pe_ltp':'ltp','finnifty_pe_volume':'volume','finnifty_pe_contract_value':'contract_value','finnifty_pe_oi':'oi','finnifty_pe_strike_type':'option_type'})

    fnf_stacked_df_all = pd.concat([fnf_ce,fnf_pe])
    fnf_stacked_df_all = fnf_stacked_df_all.sort_values(by='strike', ascending=False)
    fnf_stacked_df_all.reset_index(drop=True, inplace=True)


    #https://vega.github.io/vega-lite/examples/bar_grouped.html
    #oi
    f6_df.vega_lite_chart(fnf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'oi', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

    #ltp
    f7_df.vega_lite_chart(fnf_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'ltp', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)
#===USDINR
#==========USDINR
#=======================
#=======================
u1,u2 = st.columns(2)
u3,u4 = st.columns(2)
tu1,tu2 = st.columns(2)
tu1_df = tu1.empty()
tu2_df = tu2.empty()
u1.header("Trending oi -  USDINR - CE")
#USDINR_ce oi ============
u1_df = u1.empty()
u3_df = u3.empty()
#@st.cache(suppress_st_warning=True)
def USDINR_ce():
    USDINR_ce_oi_keys_list = r.keys("USDINR*CE_OI")
    USDINR_ce_oi_values =  r.mget(USDINR_ce_oi_keys_list)

    USDINR_ce_ticker_list = []
    USDINR_ce_strike_list = []
    USDINR_ce_strike_type_list = []
    USDINR_ce_volume = []
    USDINR_ce_ltp = []
    for i in USDINR_ce_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        USDINR_ce_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        USDINR_ce_volume.append(ticker_volume)

        USDINR_ce_ticker_list.append(ticker_name)
        USDINR_ce_strike_list.append(tmp_str[-7:-5])
        USDINR_ce_strike_type_list.append(tmp_str[-5:-3])
    USDINR_ce_oi_df = pd.DataFrame(list(zip(USDINR_ce_ticker_list,USDINR_ce_oi_values,USDINR_ce_strike_list,USDINR_ce_strike_type_list,USDINR_ce_ltp,USDINR_ce_volume)),columns=["USDINR_ce_ticker","USDINR_ce_oi",'USDINR_ce_strike','USDINR_ce_strike_type','USDINR_ce_ltp','USDINR_ce_volume'])
    USDINR_ce_oi_df = USDINR_ce_oi_df.fillna(0)
    USDINR_ce_oi_df['USDINR_ce_ltp'] = USDINR_ce_oi_df['USDINR_ce_ltp'].astype(float)
    USDINR_ce_oi_df['USDINR_ce_volume'] = USDINR_ce_oi_df['USDINR_ce_volume'].astype(int)
    USDINR_ce_oi_df['USDINR_ce_contract_value'] = (USDINR_ce_oi_df['USDINR_ce_volume'] * USDINR_ce_oi_df['USDINR_ce_ltp']).astype(int)

    USDINR_ce_oi_df['USDINR_ce_oi'] = USDINR_ce_oi_df['USDINR_ce_oi'].astype(float)
    USDINR_ce_oi_df['USDINR_ce_ticker'] = USDINR_ce_oi_df['USDINR_ce_ticker'].astype(str)
    USDINR_ce_oi_df = USDINR_ce_oi_df.sort_values(by='USDINR_ce_contract_value', ascending=False)
    USDINR_ce_oi_df.reset_index(drop=True, inplace=True)
    u1_df.dataframe(USDINR_ce_oi_df)
    USDINR_ce_oi_total = USDINR_ce_oi_df['USDINR_ce_oi'].sum()
    u3_df.write("USDINR_ce_oi_total : "+str(USDINR_ce_oi_total))
    return USDINR_ce_oi_df,USDINR_ce_oi_total
#USDINR_pe oi ============
u2.header("Trending oi -  USDINR - PE")
u2_df = u2.empty()
u4_df = u4.empty()
#@st.cache(suppress_st_warning=True)
def USDINR_pe():
    USDINR_ce_oi_df,USDINR_ce_oi_total = USDINR_ce()
    USDINR_pe_oi_keys_list = r.keys("USDINR*PE_OI")
    USDINR_pe_oi_values =  r.mget(USDINR_pe_oi_keys_list)

    USDINR_pe_ticker_list = []
    USDINR_pe_strike_list = []
    USDINR_pe_strike_type_list = []
    USDINR_pe_volume = []
    USDINR_pe_ltp = []
    for i in USDINR_pe_oi_keys_list:
        tmp_str = str(i,'utf-8')
        ticker_name  = tmp_str.replace("_OI","")

        ticker_ltp = r.get(ticker_name+"_LTP")
        USDINR_pe_ltp.append(ticker_ltp)

        ticker_volume = r.get(ticker_name+"_VOLUME")
        USDINR_pe_volume.append(ticker_volume)

        USDINR_pe_ticker_list.append(ticker_name)
        USDINR_pe_strike_list.append(tmp_str[-7:-5])
        USDINR_pe_strike_type_list.append(tmp_str[-5:-3])
    USDINR_pe_oi_df = pd.DataFrame(list(zip(USDINR_pe_ticker_list,USDINR_pe_oi_values,USDINR_pe_strike_list,USDINR_pe_strike_type_list,USDINR_pe_ltp,USDINR_pe_volume)),columns=["USDINR_pe_ticker","USDINR_pe_oi",'USDINR_pe_strike','USDINR_pe_strike_type','USDINR_pe_ltp','USDINR_pe_volume'])
    USDINR_pe_oi_df = USDINR_pe_oi_df.fillna(0)
    USDINR_pe_oi_df['USDINR_pe_ltp'] = USDINR_pe_oi_df['USDINR_pe_ltp'].astype(float)
    USDINR_pe_oi_df['USDINR_pe_volume'] = USDINR_pe_oi_df['USDINR_pe_volume'].astype(int)
    USDINR_pe_oi_df['USDINR_pe_contract_value'] = (USDINR_pe_oi_df['USDINR_pe_volume'] * USDINR_pe_oi_df['USDINR_pe_ltp']).astype(int)

    USDINR_pe_oi_df['USDINR_pe_oi'] = USDINR_pe_oi_df['USDINR_pe_oi'].astype(float)
    USDINR_pe_oi_df['USDINR_pe_ticker'] = USDINR_pe_oi_df['USDINR_pe_ticker'].astype(str)
    USDINR_pe_oi_df = USDINR_pe_oi_df.sort_values(by='USDINR_pe_contract_value', ascending=False)
    USDINR_pe_oi_df.reset_index(drop=True, inplace=True)
    u2_df.dataframe(USDINR_pe_oi_df)
    USDINR_pe_oi_total = USDINR_pe_oi_df['USDINR_pe_oi'].sum()
    u4_df.write("USDINR_pe_oi_total : "+str(USDINR_pe_oi_total))
    USDINR_trending_oi_total = USDINR_pe_oi_total - USDINR_ce_oi_total
    if(USDINR_trending_oi_total<0):
        USDINR_trending_oi_suggestion = "<font color='red'>Bearish</font>"
    else:
        USDINR_trending_oi_suggestion =  "<font color='green'>Bullish</font>"
    tu1_df.write("USDINR trending_oi_total : "+str(USDINR_trending_oi_total))
    tu2_df.markdown("USDINR trending_oi_suggestion : "+USDINR_trending_oi_suggestion, unsafe_allow_html=True)
    return USDINR_pe_oi_df,USDINR_pe_oi_total
#============all strike  - USDINR oi 
u5 = st.empty()
u5.header("USDINR - OI - CHAIN")
u5_df = st.empty()

u6,u7 = st.columns(2)
u6_df = u6.empty()
u7_df = u7.empty()
def usdinr_oi_chain_chart():
    USDINR_ce_oi_df,USDINR_ce_oi_total = USDINR_ce()
    USDINR_pe_oi_df,USDINR_pe_oi_total = USDINR_pe()
    #st.header("USDINR - OI - CHAIN")
    USDINR_all_strike_oi_df = pd.merge(USDINR_pe_oi_df, USDINR_ce_oi_df,left_on='USDINR_pe_strike',right_on='USDINR_ce_strike') #https://realpython.com/pandas-merge-join-and-concat/
    USDINR_all_strike_oi_df = USDINR_all_strike_oi_df.sort_values(by='USDINR_ce_strike', ascending=True)
    #TICKER IusdinrORMATION pe_ticker and ce_ticker below - blocked for space contract  -working
    #USDINR_all_strike_oi_df = USDINR_all_strike_oi_df [['USDINR_pe_ticker','USDINR_pe_oi','USDINR_pe_contract_value','USDINR_pe_volume','USDINR_pe_ltp','USDINR_pe_strike','USDINR_ce_ltp','USDINR_ce_volume','USDINR_ce_contract_value','USDINR_ce_oi','USDINR_ce_ticker']]
    USDINR_all_strike_oi_df = USDINR_all_strike_oi_df [['USDINR_pe_oi','USDINR_pe_contract_value','USDINR_pe_volume','USDINR_pe_ltp','USDINR_pe_strike','USDINR_ce_ltp','USDINR_ce_volume','USDINR_ce_contract_value','USDINR_ce_oi']]
    USDINR_all_strike_oi_df = USDINR_all_strike_oi_df.rename(columns={'USDINR_pe_strike': 'strike'})
    USDINR_all_strike_oi_df = USDINR_all_strike_oi_df.sort_values(by='strike', ascending=False)
    u5_df.dataframe(USDINR_all_strike_oi_df)


    #========chart - usdinr
    #usdinr
    usdinr_ce = USDINR_ce_oi_df[['USDINR_ce_strike','USDINR_ce_ltp','USDINR_ce_volume','USDINR_ce_contract_value','USDINR_ce_oi','USDINR_ce_strike_type']]
    usdinr_ce = usdinr_ce.rename(columns={'USDINR_ce_strike': 'strike','USDINR_ce_ltp':'ltp','USDINR_ce_volume':'volume','USDINR_ce_contract_value':'contract_value','USDINR_ce_oi':'oi','USDINR_ce_strike_type':'option_type'})

    usdinr_pe = USDINR_pe_oi_df[['USDINR_pe_strike','USDINR_pe_ltp','USDINR_pe_volume','USDINR_pe_contract_value','USDINR_pe_oi','USDINR_pe_strike_type']]
    usdinr_pe = usdinr_pe.rename(columns={'USDINR_pe_strike': 'strike','USDINR_pe_ltp':'ltp','USDINR_pe_volume':'volume','USDINR_pe_contract_value':'contract_value','USDINR_pe_oi':'oi','USDINR_pe_strike_type':'option_type'})

    usdinr_stacked_df_all = pd.concat([usdinr_ce,usdinr_pe])
    usdinr_stacked_df_all = usdinr_stacked_df_all.sort_values(by='strike', ascending=False)
    usdinr_stacked_df_all.reset_index(drop=True, inplace=True)


    #https://vega.github.io/vega-lite/examples/bar_grouped.html
    #oi
    u6_df.vega_lite_chart(usdinr_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'oi', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)

    #ltp
    u7_df.vega_lite_chart(usdinr_stacked_df_all, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'strike', 'type': 'quantitative'},
            'y': {'field': 'ltp', 'type': 'quantitative'},
            'xOffset': {'field': 'strike'},
            'color': {'field': 'option_type'},
        },
    },use_container_width = True,height=500)


 #INDICES=================================
i1,i2 = st.columns(2)
i1.header("Index")
i3,i4 = st.columns(2)
i3_df = i3.empty()
i4_df = i4.empty()
def index():
    indices_ticker = ['INDIA VIX','NIFTY 100','NIFTY 200','NIFTY 50','NIFTY 500','NIFTY ALPHA 50','NIFTY ALPHALOWVOL','NIFTY AUTO','NIFTY BANK','NIFTY COMMODITIES','NIFTY CONSUMPTION','NIFTY CPSE','NIFTY DIV OPPS 50','NIFTY ENERGY','NIFTY FIN SERVICE','NIFTY FMCG','NIFTY INFRA','NIFTY IT','NIFTY MEDIA','NIFTY METAL','NIFTY MID LIQ 15','NIFTY MIDCAP 100','NIFTY MIDCAP 150','NIFTY MIDCAP 50','NIFTY MIDSML 400','NIFTY MNC','NIFTY NEXT 50','NIFTY PHARMA','NIFTY PSE','NIFTY PSU BANK','NIFTY PVT BANK','NIFTY REALTY','NIFTY SERV SECTOR','NIFTY SMLCAP 100','NIFTY SMLCAP 250','NIFTY SMLCAP 50']

    ticker_ltp_key_list = []
    ticker_high_key_list = []
    ticker_low_key_list = []
    ticker_pclose_key_list = []
    ticker_open_key_list = []
    ticker_change_percentage_key_list = []

    for i in indices_ticker:
        ticker_ltp_key_list.append(i+"_LTP")
        ticker_high_key_list.append(i+"_HIGH")
        ticker_low_key_list.append(i+"_LOW")
        ticker_pclose_key_list.append(i+"_PCLOSE")
        ticker_open_key_list.append(i+"_OPEN")
        ticker_change_percentage_key_list.append(i+"_CHANGE_PERCENTAGE")

    ticker_ltp_value = r.mget(ticker_ltp_key_list)
    ticker_high_value = r.mget(ticker_high_key_list)
    ticker_low_value = r.mget(ticker_low_key_list)
    ticker_pclose_value = r.mget(ticker_pclose_key_list)
    ticker_open_value = r.mget(ticker_open_key_list)
    ticker_change_percentage_value = r.mget(ticker_change_percentage_key_list)

    df_indices = pd.DataFrame(list(zip(indices_ticker, ticker_ltp_value,ticker_change_percentage_value,ticker_open_value, ticker_high_value, ticker_low_value,ticker_pclose_value)),    columns=['indices_ticker','indices_ltp','indices_change_percentage', 'indices_open','indices_high','indices_low','indices_pclose'])
    df_indices = df_indices.fillna(0)
    df_indices['indices_ltp'] = df_indices['indices_ltp'].astype(float)
    df_indices['indices_change_percentage'] = df_indices['indices_change_percentage'].astype(float)
    df_indices['indices_open'] = df_indices['indices_open'].astype(float)
    df_indices['indices_high'] = df_indices['indices_high'].astype(float)
    df_indices['indices_low'] = df_indices['indices_low'].astype(float)
    df_indices['indices_pclose'] = df_indices['indices_pclose'].astype(float)
    df_indices = df_indices.sort_values(by='indices_change_percentage', ascending=False)
    df_indices.reset_index(drop=True, inplace=True)
    i3_df.dataframe(df_indices)

    #chart - indices ===
    i4_df.vega_lite_chart(df_indices, {
        'mark': {'type': 'bar', 'tooltip': True},
        'encoding': {
            'x': {'field': 'indices_ticker'},
            'y': {'field': 'indices_change_percentage', 'type': 'quantitative'},
            'xOffset': {'field': 'indices_ticker'},
            'color': {'field': 'indices_ticker'},
        },
    },use_container_width = True,height=500)
        
if start_button.button('Start',key='start'):
    start_button.empty()
    if st.button('Stop',key='stop'):
        pass
    while True:
        tmp_df = df_values()
        try:
            write_update_text()
        except:
            print("None Type Found in Redis.. Check Redis worker is inserting data in celery")
        #df_values()
        upper_circuit_leaders_df()
        lower_circuit_leaders_df()
        volume_leaders_df()
        volume_gain_leaders_df()
        volume_loss_leaders_df()
        contract_value_leaders_df()
        contract_gain_leaders_df()
        contract_loss_leaders_df()
        #definedge_breakout_csv_df()
        definedge_breakout_csv_scan_df()
        bnf_ce()
        bnf_pe()      
       
        bnf_oi_chain_chart()
        bnf_suggestion()
        bnf_oi_chain()

        nifty_ce()
        nifty_pe()
        nf_oi_chain_chart()

        finnifty_ce()
        finnifty_pe()
        fnf_oi_chain_chart()

        USDINR_ce()
        USDINR_pe()
        usdinr_oi_chain_chart()
        
        index()

        time.sleep(1)
