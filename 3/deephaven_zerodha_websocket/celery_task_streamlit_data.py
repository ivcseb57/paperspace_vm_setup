#trigger cmd
#celery -A celery_task_streamlit_data worker --without-heartbeat --without-gossip --without-mingle -P eventlet -Q celery_queue_task_streamlit_data -l INFO  -n worker1.%h --pool=prefork

from datetime import datetime,date
from dateutil.relativedelta import relativedelta
import pytz
tz_NY = pytz.timezone('Asia/Kolkata')
import redis

from celery import Celery
import pandas as pd
import datetime
import shutil
#from win32com.client import Dispatch #comment inside oracle instance
from threading import Thread
from flatten_json import flatten
import subprocess
from instrument_master_zerodha_finvasia_df_list import master_zerodha_finvasia_master_df
from confluent_kafka import Producer
import json

import configparser
from os import path


file_path = path.abspath(__file__)
dir_path = path.dirname(file_path)
config_file_path = path.join(dir_path, 'config.properties')

with open(config_file_path) as fp:
    cp = configparser.ConfigParser()
    cp.read_file(fp)
    meta = dict(cp.items("DEFAULT"))

container_host_machine_ip = meta["container_host_machine_ip"]
rabbitmq_port = meta["rabbitmq_port"]
redpanda_port = meta["redpanda_port"]
redis_port = meta["redis_port"]
redis_host = meta["redis_host"]

r = redis.Redis(host=f'{redis_host}', port=redis_port, db=0)
df_master = pd.read_csv("https://api.kite.trade/instruments")
df_master_index = df_master.set_index("instrument_token")


rr_master_df = master_zerodha_finvasia_master_df()

#https://github.com/celery/celery/issues/3773
app = Celery('celery_task_streamlit_data',  broker_heartbeat=0, broker=f"amqp://rr:rr@{container_host_machine_ip}:{rabbitmq_port}/rr_vhost")
app.config_from_object('celery_config_task_streamlit_data')

@app.task(name='celery_streamlit_data',ignore_result=True,typing=False)
def celery_streamlit_data(ticks):
    datetime_NY = datetime.datetime.now(tz_NY)  
    indian_time =  datetime_NY.strftime("%d_%m_%Y_%H_%M_%S_%f")
    df_indian_time =  datetime_NY.strftime("%d%m%Y %H:%M:%S")
    print("India time:",indian_time) 
    r.set("TICK_LAST_INSERT_TIME", indian_time )
    dic_flattened = [flatten(d) for d in ticks]
    df = pd.DataFrame.from_dict(dic_flattened)
    #print(df)
    df_merged_with_master = pd.merge(df, df_master_index, on='instrument_token', how='left')
    #changing time to importable amibroker format
    df_merged_with_master['last_trade_time'] = df_indian_time
#     for col in df_merged_with_master.columns:  # printing df column header
#      print(col)
    
    df_merged_with_master['abs_high_diff'] = df_merged_with_master['ohlc_high'] - df_merged_with_master['last_price_x']
    df_merged_with_master['abs_low_diff'] = df_merged_with_master['ohlc_low'] - df_merged_with_master['last_price_x']
    df_merged_with_master['total_range'] = df_merged_with_master['abs_high_diff'] + abs(df_merged_with_master['abs_low_diff'])
    if 'volume_traded' in df_merged_with_master.columns: 
      df_merged_with_master['volume_traded'] = df_merged_with_master['volume_traded'].fillna(0).astype(int)
    else:
      df_merged_with_master['volume_traded'] = 0
    df_merged_with_master['traded_contract_value'] = df_merged_with_master['volume_traded'] * df_merged_with_master['last_price_x']
    df_merged_with_master['traded_contract_value'] = df_merged_with_master['traded_contract_value'].fillna(0).astype(int)  
    
    df_merged_with_master['abs_pts'] = df_merged_with_master['ohlc_close']*((df_merged_with_master['change']/100))
    # for col in df.columns:
    #   print(col)
    if 'average_traded_price' in df_merged_with_master.columns: 
      df_filtered = df_merged_with_master[['tradingsymbol','segment','tradable','traded_contract_value','volume_traded','last_trade_time','last_price_x','average_traded_price','ohlc_open','ohlc_high','ohlc_low','ohlc_close','exchange_token','instrument_token','abs_high_diff','abs_low_diff','total_range','abs_pts','change','oi']]#.set_index("tradingsymbol")
    else:
      df_merged_with_master['average_traded_price'] = 0
      df_merged_with_master['oi'] = 0
      df_filtered = df_merged_with_master[['tradingsymbol','segment','tradable','traded_contract_value','volume_traded','last_trade_time','last_price_x','average_traded_price','ohlc_open','ohlc_high','ohlc_low','ohlc_close','exchange_token','instrument_token','abs_high_diff','abs_low_diff','total_range','abs_pts','change','oi']]#.set_index("tradingsymbol")


    #df_filtered = df_filtered.sort_values(by=['traded_contract_value','abs_low_diff','abs_high_diff'], ascending=(False,False,True))
    df_filtered = df_filtered.sort_values(by=['volume_traded'], ascending=(False))

    #===storing key
    #print("====================="+str(total_ticker)+"======================")
    #df_remove_tokens = df_filtered.loc[df_filtered['total_range'] <= 5]
    # df_remove_tokens = df_filtered.loc[df_filtered['volume_traded'] <= 100000]
    
    # if(df_remove_tokens.shape[0]!=0):
    #   df_remove_tokens_list = df_remove_tokens['instrument_token'].to_list()
    #   ws.unsubscribe(df_remove_tokens_list)
    #   print("deleted tokens : "+str(len(df_remove_tokens_list)))
      #print(df_remove_tokens_list)
      #redis db
    pipe = r.pipeline()   #https://stackoverflow.com/questions/49962361/how-to-store-the-value-of-each-element-of-pandas-data-frame-in-redis
    for index, row in df_filtered.iterrows():
      #r.set(row['tradingsymbol']+"_LTP",  row['last_price_x'])
      pipe.set(row['tradingsymbol']+"_NAME",  row['tradingsymbol'])    
      pipe.set(row['tradingsymbol']+"_LTP",  row['last_price_x'])    
      
      pipe.set(row['tradingsymbol']+"_CHANGE_PERCENTAGE",  row['change']) 
      pipe.set(row['tradingsymbol']+"_VWAP",  row['average_traded_price']) 
      pipe.set(row['tradingsymbol']+"_VOLUME",  row['volume_traded']) 
      pipe.set(row['tradingsymbol']+"_OPEN",  row['ohlc_open'])
      pipe.set(row['tradingsymbol']+"_HIGH",  row['ohlc_high'])
      pipe.set(row['tradingsymbol']+"_LOW",  row['ohlc_low'])
      pipe.set(row['tradingsymbol']+"_PCLOSE",  row['ohlc_close'])  
      pipe.set(row['tradingsymbol']+"_OI",  row['oi'])  
      results = pipe.execute() 

