#https://docs.celeryproject.org/en/stable/userguide/configuration.html - reference cmds
#amibroker_queue - 2sec

from kombu import Queue
from websocket_task_desktop import *

celery_queue = 'websocket_celery_redis_insert_task_queue_desktop1' #change queue name here to change rabbitmq queue
app.conf.task_default_queue = celery_queue

# CELERY_IGNORE_RESULT = True
# CELERY_SEND_EVENTS = False
# CELERY_RESULT_BACKEND = None
# CELERY_ACKS_LATE=True
# CELERY_EVENT_QUEUE_TTL= 1

task_ignore_result = True
worker_send_task_events = False
result_backend = None
task_acks_late = True
event_queue_ttl = 1

from kombu import Exchange, Queue
# CELERY_QUEUES = [
#                      Queue(
#                          celery_queue,
#                          Exchange(celery_queue,delivery_mode=1),
#                          routing_key = celery_queue,
#                          queue_arguments = {
#                                                 'x-message-ttl': 1000
#                                            }
#                      ),
#                     Queue('priority_high')
#                 ]

task_queues = [
                     Queue(
                         celery_queue,
                         Exchange(celery_queue,delivery_mode=1),
                         routing_key = celery_queue,
                         queue_arguments = {
                                                'x-message-ttl': 1000
                                           }
                     ),
                    Queue('priority_high')
                ]
