#https://docs.celeryproject.org/en/stable/userguide/configuration.html - reference cmds
#amibroker_queue - 2sec

from kombu import Queue
from celery_task_streamlit_data import *

celery_queue = 'celery_queue_task_streamlit_data' #change queue name here to change rabbitmq queue
app.conf.task_default_queue = celery_queue

task_ignore_result = True
worker_send_task_events = False
result_backend = None
task_acks_late = True
event_queue_ttl = 1
broker_heartbeat=0

from kombu import Exchange, Queue

task_queues = [
                     Queue(
                         celery_queue,
                         Exchange(celery_queue,delivery_mode=1),
                         routing_key = celery_queue,
                         queue_arguments = {
                                                'x-message-ttl': 1000
                                           }
                     ),
                    Queue('priority_high')
                ]
