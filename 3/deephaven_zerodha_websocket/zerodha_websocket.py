import logging
import sys
from kiteconnect import KiteTicker
import pandas as pd
pd.set_option('display.max_columns', None)
from flatten_json import flatten
import redis
from datetime import datetime
import pytz    
tz_NY = pytz.timezone('Asia/Kolkata')   
from instrument_master_zerodha_finvasia_df_list import master_zerodha_finvasia_master_list, zerodha_df_nse_eq_list
# from websocket_task import celery_websocket
from websocket_task_desktop import  websocket_to_kafka # celery_websocket_desktop
from celery_task_streamlit_data import celery_streamlit_data
import urllib.parse
import configparser
from os import path


file_path = path.abspath(__file__)
dir_path = path.dirname(file_path)
config_file_path = path.join(dir_path, 'config.properties')

with open(config_file_path) as fp:
    cp = configparser.ConfigParser()
    cp.read_file(fp)
    meta = dict(cp.items("DEFAULT"))

zerodha_enctoken = meta["zerodha_enctoken"]

# all_tokens = master_zerodha_finvasia_master_list()
all_tokens = zerodha_df_nse_eq_list()

# r = redis.Redis(host='localhost', port=6379, db=0)
logging.basicConfig(level=logging.DEBUG)
# kws = KiteTicker("kitefront", "L5tmmOkkqpLkfgW3L9vy6zlojnunhpLJ3pm8Wo3bvp5iU7Y/U1lm1XkICmvPn6fF4ulfHNKeklLBuCUwtEr8Gvm90m2f1agDpnJI/CoNCp5eFMZvtn9WTQ==&user_id=PR1060",root='wss://ws.zerodha.com',debug=True)
# browser_enctoken = "0yHKzkWRrF4LBSxR/8FfJy/0hEvnYYCsiTsU8DCk9dUj3MoJFXa3VBdaZXnXBqJAcnX+IS7VPN/3GAnOfShw2UZ7ZHd4z5WUgjWuV1kZrcn5ntG8C2b8ww=="
browser_enctoken = zerodha_enctoken
encoded_enctoken = urllib.parse.quote_plus(browser_enctoken)


try:  
    kws = KiteTicker("kitefront", encoded_enctoken, root='wss://ws.zerodha.com',debug=True)
except:
    sys.exit(0)

        
def on_ticks(ws, ticks):
#   celery_websocket.delay(ticks)
  # celery_websocket_desktop.delay(ticks)
  celery_streamlit_data.delay(ticks) # streamlit data
  websocket_to_kafka.delay(ticks)
  print(ticks)
  datetime_NY = datetime.now(tz_NY)  
  indian_time =  datetime_NY.strftime("%d_%m_%Y_%H_%M_%S_%f")
  df_indian_time =  datetime_NY.strftime("%d%m%Y %H:%M:%S")
  print(indian_time)
  print("=================================")


def on_connect(ws, response):
    ws.subscribe(all_tokens)
    # ws.subscribe([290819,3035651,3036163])
    ws.set_mode(ws.MODE_FULL, all_tokens)

def on_close(ws, code, reason):
    ws.stop()

kws.on_ticks = on_ticks
kws.on_connect = on_connect
kws.on_close = on_close
kws.connect()