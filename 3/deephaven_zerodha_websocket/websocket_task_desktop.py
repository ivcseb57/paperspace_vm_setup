#celery -A websocket_task_desktop worker --without-heartbeat --without-gossip --without-mingle -P eventlet -l INFO -n worker1.%h
#celery -A websocket_task_desktop worker --without-heartbeat --without-gossip --without-mingle -P eventlet -l INFO

from datetime import datetime,date
from dateutil.relativedelta import relativedelta
import pytz
tz_NY = pytz.timezone('Asia/Kolkata')
import redis
r = redis.Redis(host='localhost', port=6379, db=0)
from celery import Celery
import pandas as pd
import datetime
import shutil
#from win32com.client import Dispatch #comment inside oracle instance
from threading import Thread
from flatten_json import flatten
import subprocess
from instrument_master_zerodha_finvasia_df_list import master_zerodha_finvasia_master_df
from confluent_kafka import Producer
import json



rr_master_df = master_zerodha_finvasia_master_df()

# app = Celery('websocket_task_desktop',  broker='amqp://rr:rr@140.238.255.151:5672/rr_vhost')
app = Celery('websocket_task_desktop',  broker='amqp://rr:rr@rr_rabbitmq_deephaven:5672/rr_vhost')

app.config_from_object('websocket_task_config_desktop')

# @app.task(ignore_result=True, typing=False)
# def celery_websocket_desktop(ticks):
#     # print(ticks)
#     # https://stackoverflow.com/questions/40860457/improve-pandas-merge-performance
#     ##subscription_df = pd.read_csv("F:\\PycharmProjects\\Amibroker_Data_Stream\\ab_csv\\zerodha_websocket_subscribe_list.csv",names=["instrument_token"]).set_index('instrument_token')
#     # zerodha_instrument_master_subscription_df = pd.merge(subscription_df, zerodha_all_instrument_master_df, on='instrument_token').set_index("instrument_token")

#     df = pd.DataFrame([flatten(tick, root_keys_to_ignore={'tradable', 'mode', 'buy_quantity', 'sell_quantity', #'ohlc',
#                                                           'last_quantity', 'last_trade_time', 'timestamp'}) for tick in
#                        ticks]).set_index("instrument_token").merge(rr_master_df, left_index=True,right_index=True) #need to change here to use the correct df master to get later values inside csv note

#     #df.to_csv("R:\\test_token_full_tmp.csv", sep=',', index=True)
#     df = df.reset_index()
#     df['last_trade_time'] = datetime.datetime.now().strftime("%Y%m%d %H%M%S")
#     df['change'] = df['change'].astype(float).apply(lambda x:round(x,3))

#     # for col in df.columns:  # printing df column header
#     #     print(col)

#     try:
#         tmp_aux3_list = ['change', 'average_traded_price',
#                          # 'buy_quantity', 'sell_quantity',
#                          'ohlc_open', 'ohlc_high', 'ohlc_low', 'ohlc_close',
#                          # 'last_quantity',
#                          #'oi_day_high', 'oi_day_low',
#                          'depth_buy_0_quantity', 'depth_buy_0_price',  # 'depth_buy_0_orders',
#                          'depth_buy_1_quantity', 'depth_buy_1_price',  # 'depth_buy_1_orders',
#                          #'depth_buy_2_quantity', 'depth_buy_2_price',  # 'depth_buy_2_orders',
#                          #'depth_buy_3_quantity', 'depth_buy_3_price',  # 'depth_buy_3_orders',
#                          #'depth_buy_4_quantity', 'depth_buy_4_price',  # 'depth_buy_4_orders',
#                          'depth_sell_0_quantity', 'depth_sell_0_price',  # 'depth_sell_0_orders',
#                          'depth_sell_1_quantity', 'depth_sell_1_price',  # 'depth_sell_1_orders',
#                          #'depth_sell_2_quantity', 'depth_sell_2_price',  # 'depth_sell_2_orders',
#                          #'depth_sell_3_quantity', 'depth_sell_3_price',  # 'depth_sell_3_orders',
#                          #'depth_sell_4_quantity', 'depth_sell_4_price',  # 'depth_sell_4_orders'
#                          ]
#         df[tmp_aux3_list] = df[tmp_aux3_list].astype(str)
#         df['aux3'] = df[tmp_aux3_list].apply(lambda x: '|'.join(x), axis=1)
#         #==tokens
#         tokens = ['instrument_token','exchange_token','finvasia_actual_leverage'] #exchange_token used as index and merged, so not available and using the copy
#         df[tokens] = df[tokens].astype(str)
#         df['tokens_all'] = df[tokens].apply(lambda x: '|'.join(x), axis=1)

#         #final_df = df[['tradingsymbol', 'last_price', 'last_trade_time', 'volume_traded', 'oi', 'tokens_all','aux3','average_traded_price']] #- zerodha format
#         final_df = df[['tradingsymbol', 'last_price_x', 'last_trade_time', 'volume_traded', 'oi', 'tokens_all', 'aux3','average_traded_price','finvasia_tradingsymbol']]  # - finvasia format finvasia_trading_symbol
#         #final_df = df[['tradingsymbol', 'last_price', 'last_trade_time', 'volume_traded', 'oi', 'tokens_all', 'aux3', 'average_traded_price', 'finvasia_trading_symbol','finvasia_actual_leverage']]  # - finvasia format finvasia_trading_symbol finvasia_actual_leverage , leverage using inside variable tokens see above 4 lines
#         #print(final_df)
#     except Exception as e:
#         if 'volume_traded' in df.columns:
#             final_df = df[['tradingsymbol', 'last_price_x', 'last_trade_time', 'volume_traded']]
#         else:
#             final_df = df[['tradingsymbol', 'last_price_x', 'last_trade_time']]

#         print("Data Error,Keys not present in the websocket data!!!")
#         print(e)

#     finally:  # any ways execute
#         # final_df.to_csv("R:\\full_final_df_tmp.csv", sep=',', index=False)
#         final_df['tradingsymbol'] = final_df['tradingsymbol'].str.replace(' ', '_')
#         # tmp_final_df = final_df
#         # tmp_final_df = tmp_final_df.sort_values(by=['volume'], ascending=False).to_csv("Y:\\RAM_DISK\\test_token_full_volume_sorted.csv", sep=',', index=False)
#         final_df = final_df.sort_values(by=['tradingsymbol'], ascending=True).to_csv("R:\\Amibroker_Data_Stream_Ram_Disk_Tick_Data.csv", sep=',',index=False)
#         # Thread(target=shutil.copy, args=["R:\\Amibroker_Data_Stream_Ram_Disk_Tick_Data.csv", "R:\\Amibroker_Data_Stream_Ram_Disk_Tick_Data_v2.csv"]).start()
     
@app.task(ignore_result=True, typing=False)
def websocket_to_kafka(ticks):
    # df = pd.DataFrame([flatten(tick}) for tick in ticks]).set_index("instrument_token").merge(rr_master_df, left_index=True,right_index=True) #need to change here to use the correct df master to get later values inside csv note

    # for col in df.columns:  # printing df column header
    #     print(col)
    for tick in ticks:
        # tmp_websocket_data = flatten(d)
        # tick['exchange_timestamp'] = tick['exchange_timestamp'].strftime("%Y-%m-%dT%H:%M:%S UTC")
        # tick['last_trade_time'] = tick['last_trade_time'].strftime("%Y-%m-%dT%H:%M:%S UTC")  
        tmp_websocket_data = flatten(tick)
        
        # print(type(tmp_websocket_data))
        # producer = Producer({"bootstrap.servers":'140.238.255.151:29092'})
        producer = Producer({"bootstrap.servers":'redpanda:29092'})

        # producer.produce(topic = 'zerodha_websocket_data_stream_topic',key = None,value = json.dumps(tmp_websocket_data))        
        producer.produce(topic = 'zerodha_websocket_data_stream_topic',key = None,value = json.dumps(tmp_websocket_data, indent=4, sort_keys=True, default=str))
        # producer.produce(topic = 'zerodha_websocket_data_stream_topic',key = None,value = tmp_websocket_data)
        
        producer.flush()
    print("pushing done to kafka !!!")


# @app.task(ignore_result=True, typing=False)
# def dummy_websocket_to_kafka(ticks):
#     # df = pd.DataFrame([flatten(tick}) for tick in ticks]).set_index("instrument_token").merge(rr_master_df, left_index=True,right_index=True) #need to change here to use the correct df master to get later values inside csv note

#     # for col in df.columns:  # printing df column header
#     #     print(col)
#     for tick in ticks:
#         # tmp_websocket_data = flatten(d)
#         # tick['exchange_timestamp'] = tick['exchange_timestamp'].strftime("%Y-%m-%dT%H:%M:%S UTC")
#         # tick['last_trade_time'] = tick['last_trade_time'].strftime("%Y-%m-%dT%H:%M:%S UTC")  
#         tmp_websocket_data = flatten(tick)
        
#         # print(type(tmp_websocket_data))
#         producer = Producer({"bootstrap.servers":'140.238.255.151:29092'})
#         # producer.produce(topic = 'zerodha_websocket_data_stream_topic',key = None,value = json.dumps(tmp_websocket_data))        
#         producer.produce(topic = 'zerodha_websocket_data_stream_topic_dummy',key = None,value = json.dumps(tmp_websocket_data, indent=4, sort_keys=True, default=str))
#         # producer.produce(topic = 'zerodha_websocket_data_stream_topic',key = None,value = tmp_websocket_data)
#         print("pushing done !!!")
#         producer.flush()