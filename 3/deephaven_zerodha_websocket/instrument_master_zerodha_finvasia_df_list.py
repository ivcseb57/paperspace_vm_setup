#file used to download master df from zerodha and finvasia and merge them to get the finvasia tradingsymbol
import pandas as pd
zerodha_df_master = pd.read_csv("https://api.kite.trade/instruments")
import requests as req
import numpy as np
# https://deephaven.io/core/pydoc/code/deephaven.pandas.html
# import deephaven.pandas as dhpd
#--downloading finvasia eq leverage
url = "https://prism.finvasia.com:9000/api/website/marginCalculator"
resp = req.get(url)
data = resp.json()['data']['LEVERAGE']
finvasia_marin_nse_df = pd.DataFrame(data)

finvasia_marin_nse_df1 = pd.DataFrame(finvasia_marin_nse_df["EQ"].to_list(),columns=['tradingsymbol','segment','leverage'])#.set_index("symbol")
finvasia_marin_nse_df1["leverage"] = finvasia_marin_nse_df1["leverage"].astype(float) #float
finvasia_marin_nse_df1["finvasia_actual_leverage"] = 100 / finvasia_marin_nse_df1["leverage"]
finvasia_marin_nse_df1["finvasia_actual_leverage"] = finvasia_marin_nse_df1['finvasia_actual_leverage'].apply(np.ceil) # df2["finvasia_actual_leverage"].astype(int)
finvasia_marin_nse_df1["tradingsymbol"] = "NSEEQ_"+finvasia_marin_nse_df1['tradingsymbol']
finvasia_marin_nse_df1 = finvasia_marin_nse_df1[["tradingsymbol","finvasia_actual_leverage"]]
#--downloading finvasia eq leverage

zerodha_df_master = pd.read_csv("https://api.kite.trade/instruments")
zerodha_df_master = zerodha_df_master.fillna("-")
# dh_zerodha_df_master = dhpd.to_table(zerodha_df_master, cols=None)
zerodha_df_master["tradingsymbol"] = zerodha_df_master["exchange"] + zerodha_df_master["instrument_type"] +"_"+ zerodha_df_master["tradingsymbol"]

zerodha_df_nse = zerodha_df_master.loc[zerodha_df_master['exchange'] == "NSE"]
finvasia_df_nse = pd.read_csv("https://shoonya.finvasia.com/NSE_symbols.txt.zip", compression='zip')
dict = {'Exchange': 'finvasia_exchange','Token': 'finvasia_exchange_token','LotSize': 'finvasia_lot_size','Symbol':'finvasia_name','TradingSymbol':'finvasia_tradingsymbol','Instrument':'finvasia_instrument_type','TickSize':'finvasia_tick_size'}
finvasia_df_nse.drop(finvasia_df_nse.columns[len(finvasia_df_nse.columns)-1], axis=1, inplace=True)
finvasia_df_nse.rename(columns=dict, inplace=True)
#merged_nse_df = pd.merge(zerodha_df_nse, finvasia_df_nse,  how='left', left_on=['exchange_token','name','instrument_type','tick_size'], right_on = ['finvasia_exchange_token','finvasia_name','finvasia_instrument_type','finvasia_tick_size'])
merged_nse_df = pd.merge(zerodha_df_nse, finvasia_df_nse,  how='left', left_on=['exchange_token','tick_size'], right_on = ['finvasia_exchange_token','finvasia_tick_size'])
#filter from df
tmp_merged_nse_eq_df = merged_nse_df.loc[(merged_nse_df['finvasia_instrument_type'] == "EQ") & (merged_nse_df['finvasia_tick_size'] == 0.05)].reset_index(drop=True)
tmp_merged_nse_eq_df = pd.merge(tmp_merged_nse_eq_df,finvasia_marin_nse_df1,how="left", on="tradingsymbol") #merging finvasia nse leverage
zerodha_df_nse_eq_list1 = tmp_merged_nse_eq_df['instrument_token'].to_list()

tmp_merged_nse_etf_df = merged_nse_df.loc[(merged_nse_df['finvasia_instrument_type'] == "EQ") & (merged_nse_df['finvasia_tick_size'] == 0.01)].reset_index(drop=True)
zerodha_df_nse_etf_list1 = tmp_merged_nse_etf_df['instrument_token'].to_list()

tmp_merged_nse_index_df = merged_nse_df.loc[(merged_nse_df['segment'] == "INDICES")].reset_index(drop=True)
zerodha_df_nse_index_list1 = tmp_merged_nse_index_df['instrument_token'].to_list()

zerodha_df_nse_eq = zerodha_df_master.loc[(zerodha_df_master['exchange'] == "NSE") & (zerodha_df_master['tick_size'] == 0.05)  & (zerodha_df_master['name'] != '-')]

#---nfo
zerodha_df_nfo = zerodha_df_master.loc[zerodha_df_master['exchange'] == "NFO"]
finvasia_df_nfo = pd.read_csv("https://shoonya.finvasia.com/NFO_symbols.txt.zip", compression='zip')
dict = {'Exchange': 'finvasia_exchange','Token': 'finvasia_exchange_token','LotSize': 'finvasia_lot_size','Symbol':'finvasia_name','TradingSymbol':'finvasia_tradingsymbol','Instrument':'finvasia_instrument_type','TickSize':'finvasia_tick_size'}
finvasia_df_nfo.drop(finvasia_df_nfo.columns[len(finvasia_df_nfo.columns)-1], axis=1, inplace=True)
finvasia_df_nfo.rename(columns=dict, inplace=True)
merged_nfo_df = pd.merge(zerodha_df_nfo, finvasia_df_nfo,  how='left', left_on=['exchange_token','tick_size'], right_on = ['finvasia_exchange_token','finvasia_tick_size'])
merged_nfo_df['expiry'] = pd.to_datetime(merged_nfo_df['expiry'], format='%Y-%m-%d')
#todo check merged_nfo_df for optidx for both nifty and banknifty
#filter from df
#==== optidx
tmp_merged_nfo_index_option_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTIDX") & (merged_nfo_df['finvasia_tick_size'] == 0.05)].reset_index(drop=True)

tmp_merged_nfo_index_option_nifty_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTIDX") & (merged_nfo_df['finvasia_tick_size'] == 0.05) & (merged_nfo_df['name'] == "NIFTY")].reset_index(drop=True)
tmp_merged_nfo_index_option_banknifty_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTIDX") & (merged_nfo_df['finvasia_tick_size'] == 0.05) & (merged_nfo_df['name'] == "BANKNIFTY")].reset_index(drop=True)
tmp_merged_nfo_index_option_finnifty_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTIDX") & (merged_nfo_df['finvasia_tick_size'] == 0.05) & (merged_nfo_df['name'] == "FINNIFTY")].reset_index(drop=True)
tmp_merged_nfo_index_option_midcpnifty_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTIDX") & (merged_nfo_df['finvasia_tick_size'] == 0.05) & (merged_nfo_df['name'] == "MIDCPNIFTY")].reset_index(drop=True)


nifty_earliest_date = tmp_merged_nfo_index_option_nifty_df['expiry'].min()
tmp_merged_nfo_index_option_nifty_df_2 = tmp_merged_nfo_index_option_nifty_df.loc[tmp_merged_nfo_index_option_nifty_df['expiry'] == nifty_earliest_date]
zerodha_df_nfo_nifty_earliest_options_list1 = tmp_merged_nfo_index_option_nifty_df_2['instrument_token'].to_list()

banknifty_earliest_date = tmp_merged_nfo_index_option_banknifty_df['expiry'].min()
tmp_merged_nfo_index_option_banknifty_df_2 = tmp_merged_nfo_index_option_banknifty_df.loc[tmp_merged_nfo_index_option_banknifty_df['expiry'] == banknifty_earliest_date]
zerodha_df_nfo_banknifty_earliest_options_list1 = tmp_merged_nfo_index_option_banknifty_df_2['instrument_token'].to_list()

finnifty_earliest_date = tmp_merged_nfo_index_option_finnifty_df['expiry'].min()
tmp_merged_nfo_index_option_finnifty_df_2 = tmp_merged_nfo_index_option_finnifty_df.loc[tmp_merged_nfo_index_option_finnifty_df['expiry'] == finnifty_earliest_date]
zerodha_df_nfo_finnifty_earliest_options_list1 = tmp_merged_nfo_index_option_finnifty_df_2['instrument_token'].to_list()

midcpnifty_earliest_date = tmp_merged_nfo_index_option_midcpnifty_df['expiry'].min()
tmp_merged_nfo_index_option_midcpnifty_df_2 = tmp_merged_nfo_index_option_midcpnifty_df.loc[tmp_merged_nfo_index_option_midcpnifty_df['expiry'] == midcpnifty_earliest_date]
zerodha_df_nfo_midcpnifty_earliest_options_list1 = tmp_merged_nfo_index_option_midcpnifty_df_2['instrument_token'].to_list()
#==== optidx
#==== optstk
tmp_merged_nfo_stock_option_df = merged_nfo_df.loc[(merged_nfo_df['finvasia_instrument_type'] == "OPTSTK") & (merged_nfo_df['finvasia_tick_size'] == 0.05)].reset_index(drop=True)
#todo do this for individual stock option
#==== optstk
#---bse
zerodha_df_bse = zerodha_df_master.loc[zerodha_df_master['exchange'] == "BSE"]
zerodha_df_bse_list1 = zerodha_df_bse['instrument_token'].to_list()

zerodha_df_bcd = zerodha_df_master.loc[zerodha_df_master['exchange'] == "BCD"]
zerodha_df_bcd_list1 = zerodha_df_bcd['instrument_token'].to_list()

#---cds
zerodha_df_cds = zerodha_df_master.loc[zerodha_df_master['exchange'] == "CDS"]
finvasia_df_cds = pd.read_csv("https://shoonya.finvasia.com/CDS_symbols.txt.zip", compression='zip')
dict = {'Exchange': 'finvasia_exchange','Token': 'finvasia_exchange_token','LotSize': 'finvasia_lot_size','Symbol':'finvasia_name','TradingSymbol':'finvasia_tradingsymbol','Instrument':'finvasia_instrument_type','TickSize':'finvasia_tick_size'}
finvasia_df_cds.drop(finvasia_df_cds.columns[len(finvasia_df_cds.columns)-1], axis=1, inplace=True)
finvasia_df_cds.rename(columns=dict, inplace=True)
merged_cds_df = pd.merge(zerodha_df_cds, finvasia_df_cds,  how='left', left_on=['exchange_token','tick_size'], right_on = ['finvasia_exchange_token','finvasia_tick_size'])
tmp_merged_cds_df = merged_cds_df.loc[(merged_cds_df['finvasia_instrument_type'] == "OPTIDX") & (merged_cds_df['finvasia_tick_size'] == 0.05)].reset_index(drop=True)
#todo this for 4 currency pairs in name field
tmp_merged_cds_eurinr_df = merged_cds_df.loc[(merged_cds_df['name'] == "EURINR")].reset_index(drop=True)
tmp_merged_cds_gbpinr_df = merged_cds_df.loc[(merged_cds_df['name'] == "GBPINR")].reset_index(drop=True)
tmp_merged_cds_jpyinr_df = merged_cds_df.loc[(merged_cds_df['name'] == "JPYINR")].reset_index(drop=True)
tmp_merged_cds_usdinr_df = merged_cds_df.loc[(merged_cds_df['name'] == "USDINR")].reset_index(drop=True)

eurinr_earliest_date = tmp_merged_cds_eurinr_df['expiry'].min()
tmp_merged_cds_eurinr_df_2 = tmp_merged_cds_eurinr_df.loc[tmp_merged_cds_eurinr_df['expiry'] == eurinr_earliest_date]
zerodha_df_cds_eurinr_earliest_options_list1 = tmp_merged_cds_eurinr_df_2['instrument_token'].to_list()

gbpinr_earliest_date = tmp_merged_cds_gbpinr_df['expiry'].min()
tmp_merged_cds_gbpinr_df_2 = tmp_merged_cds_gbpinr_df.loc[tmp_merged_cds_gbpinr_df['expiry'] == gbpinr_earliest_date]
zerodha_df_cds_gbpinr_earliest_options_list1 = tmp_merged_cds_gbpinr_df_2['instrument_token'].to_list()

jpyinr_earliest_date = tmp_merged_cds_jpyinr_df['expiry'].min()
tmp_merged_cds_jpyinr_df_2 = tmp_merged_cds_jpyinr_df.loc[tmp_merged_cds_jpyinr_df['expiry'] == jpyinr_earliest_date]
zerodha_df_cds_jpyinr_earliest_options_list1 = tmp_merged_cds_jpyinr_df_2['instrument_token'].to_list()

usdinr_earliest_date = tmp_merged_cds_usdinr_df['expiry'].min()
tmp_merged_cds_usdinr_df_2 = tmp_merged_cds_usdinr_df.loc[tmp_merged_cds_usdinr_df['expiry'] == usdinr_earliest_date]
zerodha_df_cds_usdinr_earliest_options_list1 = tmp_merged_cds_usdinr_df_2['instrument_token'].to_list()

#---cds

zerodha_df_mcx = zerodha_df_master.loc[zerodha_df_master['exchange'] == "MCX"]
finvasia_df_mcx = pd.read_csv("https://shoonya.finvasia.com/MCX_symbols.txt.zip", compression='zip')
dict = {'Exchange': 'finvasia_exchange','Token': 'finvasia_exchange_token','LotSize': 'finvasia_lot_size','Symbol':'finvasia_name','TradingSymbol':'finvasia_tradingsymbol','Instrument':'finvasia_instrument_type','TickSize':'finvasia_tick_size'}
finvasia_df_mcx.drop(finvasia_df_mcx.columns[len(finvasia_df_mcx.columns)-1], axis=1, inplace=True)
finvasia_df_mcx.rename(columns=dict, inplace=True)

merged_mcx_df = pd.merge(zerodha_df_mcx, finvasia_df_mcx,  how='left', left_on=['exchange_token','tick_size'], right_on = ['finvasia_exchange_token','finvasia_tick_size'])
tmp_merged_mcx_fut_df = merged_mcx_df.loc[(merged_mcx_df['finvasia_tick_size'] == 0.05) & (merged_mcx_df['segment'] == "MCX-FUT")].reset_index(drop=True)

mcx_fut_earliest_date = tmp_merged_mcx_fut_df['expiry'].min()
tmp_merged_mcx_fut_df_2 = tmp_merged_mcx_fut_df.loc[tmp_merged_mcx_fut_df['expiry'] == mcx_fut_earliest_date]
zerodha_df_mcx_fut_earliest_options_list1 = tmp_merged_mcx_fut_df_2['instrument_token'].to_list()


def zerodha_df_nse_eq_list():
    return zerodha_df_nse_eq_list1

def zerodha_df_nse_index_list():
    return zerodha_df_nse_index_list1

def zerodha_df_nfo_nifty_earliest_options_list():
    return zerodha_df_nfo_nifty_earliest_options_list1

def zerodha_df_nfo_banknifty_earliest_options_list():
    return zerodha_df_nfo_banknifty_earliest_options_list1

def zerodha_df_nfo_finnifty_earliest_options_list():
    return zerodha_df_nfo_finnifty_earliest_options_list1

def zerodha_df_nfo_midcpnifty_earliest_options_list():
    return zerodha_df_nfo_midcpnifty_earliest_options_list1

def zerodha_df_bse_list():
    return zerodha_df_bse_list1

def zerodha_df_bcd_list():
    return zerodha_df_bcd_list1

def zerodha_df_cds_eurinr_earliest_options_list():
    return zerodha_df_cds_eurinr_earliest_options_list1

def zerodha_df_cds_gbpinr_earliest_options_list():
    return zerodha_df_cds_gbpinr_earliest_options_list1

def zerodha_df_cds_jpyinr_earliest_options_list():
    return zerodha_df_cds_jpyinr_earliest_options_list1

def zerodha_df_cds_usdinr_earliest_options_list():
    return zerodha_df_cds_usdinr_earliest_options_list1

def zerodha_df_mcx_fut_earliest_options_list():
    return zerodha_df_mcx_fut_earliest_options_list1


def master_zerodha_finvasia_master_df():
    # tmp_df = tmp_merged_nse_eq_df.append(tmp_merged_nfo_index_option_nifty_df_2, ignore_index=True) #tmp_merged_nse_etf_df
    # tmp_df = tmp_merged_nse_eq_df.concat(tmp_merged_nfo_index_option_nifty_df_2, ignore_index=True) #tmp_merged_nse_etf_df
    tmp_df = pd.concat([tmp_merged_nse_eq_df, tmp_merged_nfo_index_option_nifty_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_nse_index_df, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_nse_index_df], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_nse_etf_df, ignore_index=True) #tmp_merged_nfo_index_option_nifty_df_2
    tmp_df = pd.concat([tmp_df, tmp_merged_nse_etf_df], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_nfo_index_option_banknifty_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_nfo_index_option_banknifty_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_nfo_index_option_finnifty_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_nfo_index_option_finnifty_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_nfo_index_option_midcpnifty_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_nfo_index_option_midcpnifty_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(zerodha_df_bse, ignore_index=True)
    tmp_df = pd.concat([tmp_df, zerodha_df_bse], ignore_index=True)

    # tmp_df = tmp_df.concat(zerodha_df_bcd, ignore_index=True)
    tmp_df = pd.concat([tmp_df, zerodha_df_bcd], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_cds_eurinr_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_cds_eurinr_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_cds_gbpinr_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_cds_gbpinr_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_cds_jpyinr_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_cds_jpyinr_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_cds_usdinr_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_cds_usdinr_df_2], ignore_index=True)

    # tmp_df = tmp_df.concat(tmp_merged_mcx_fut_df_2, ignore_index=True)
    tmp_df = pd.concat([tmp_df, tmp_merged_mcx_fut_df_2], ignore_index=True)


    tmp_df = tmp_df.set_index("instrument_token")



    return tmp_df

def master_zerodha_finvasia_master_list():
    tmp_list = []
    tmp_list += zerodha_df_nse_eq_list1
    #tmp_list += zerodha_df_nse_etf_list1
    tmp_list += zerodha_df_nse_index_list1
    tmp_list += zerodha_df_nfo_nifty_earliest_options_list1
    tmp_list += zerodha_df_nfo_banknifty_earliest_options_list1
    #tmp_list += zerodha_df_nfo_finnifty_earliest_options_list1
    #tmp_list += zerodha_df_nfo_midcpnifty_earliest_options_list1
    #tmp_list += zerodha_df_bse_list1
    #tmp_list += zerodha_df_bcd_list1
    #tmp_list += zerodha_df_cds_eurinr_earliest_options_list1
    #tmp_list += zerodha_df_cds_gbpinr_earliest_options_list1
    #tmp_list += zerodha_df_cds_jpyinr_earliest_options_list1
    tmp_list += zerodha_df_cds_usdinr_earliest_options_list1
    #tmp_list += zerodha_df_mcx_fut_earliest_options_list1
    return tmp_list


# z = master_zerodha_finvasia_master_df()
# z1 = master_zerodha_finvasia_master_list()
# z= 0