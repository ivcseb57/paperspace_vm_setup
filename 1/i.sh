 #--- github link
#  git clone https://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@github.com/raaghulr/paperspace_vm_setup.git

#-- change the permission of the fle
# chmod +x docker_install.sh

#-- test the docker run
# docker run hello-world
# sudo docker run hello-world
# sudo docker run --name docker-nginx -p 80:80 nginx

#-- docker remove all images
# docker rmi $(docker images -a -q) -f

# docker exec zerotier-one zerotier-cli join 8056c2e21c35d12b

#-- stop all containers
# docker ps -aq | xargs docker stop | xargs docker rm
 
 #curl -O https://gist.githubusercontent.com/raaghulr/9d564baea69a4bb6c2098d276b6ffbe1/raw/491ade57a5794650dc6c0bd52899181ab6f5f8e1/paperspace_docker_install.sh
 #chmod +x paperspace_docker_install.sh
 #./paperspace_docker_install.sh
echo "---started updating the kernel---"
echo "---updating the docker repository---"
# sudo apt-get update -y
sudo apt-get install ca-certificates curl gnupg lsb-release -y
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
echo "---updating the the kernel repository---"
echo "---installing the docker---"
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y
echo "---starting to the docker---"
sudo service docker start
echo "---starting to the docker---"
sudo service docker start
echo "---stopping to the docker---"
sudo service docker stop
echo "---changing the docker storage driver---"
cat <<EOF >/etc/docker/daemon.json
{
    "storage-driver":"vfs"
}
EOF
echo "---starting to the docker---"
sudo service docker start

# Install additional packages
echo "--- Installing additional packages ---"
sudo apt-get install unzip iputils-ping htop docker-compose -y

echo "--- Script execution complete ---"

echo "---change the zerotier folder---"
cd ..
cd 2
docker compose up -d
echo "---connecting the zerotier network 8056c2e21c35d12b ---"
docker exec zerotier-one zerotier-cli join 8056c2e21c35d12b
echo "---zerotier connectivity successfull---"

echo "---starting the visual studio code server---"
cd ..
cd 3
cd code_server
docker compose up -d
echo "---code server up and running---"

echo "---logging to the gitlab---"
docker login registry.gitlab.com -u ivcseb57 -p r284619735@
# docker build -t registry.gitlab.com/ivcseb57/paperspace_vm_setup .
# docker push registry.gitlab.com/ivcseb57/paperspace_vm_setup
echo "---logging to the gitlab completed---"